#!/usr/bin/env bash
# Base Version
BASE_VERSION='1.0.0-dev'

# Set Version
AURORA_BUILD_VERSION=$(date '+%Y%m%d%H%M%S')
bump2version --new-version "${BASE_VERSION}${AURORA_BUILD_VERSION}" --no-commit --no-tag release
