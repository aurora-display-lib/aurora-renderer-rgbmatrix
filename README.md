# RGB Matrix Renderer for Aurora Display Library
This package contains a renderer using Henner Zeller's
[rpi-rgb-led-matrix](https://github.com/hzeller/rpi-rgb-led-matrix)
library.

> **Note**: This package will not provide you `rpi-rgb-led-matrix` as a
> dependency. You will have to download and compile the bindings yourself
> using the instructions provided here:
> https://github.com/hzeller/rpi-rgb-led-matrix/blob/master/bindings/python/README.md
